# CS205 Final

## Name
GUI Bullshit Card Game

## Description
Our project is a GUI game that recreates the classic card game "Bullshit". We want to allow the user to play against computer players with different difficulties.

## Instructions
Run src/app.py

`pip install -r requirements.txt`

`python src/app.py`

## Authors and acknowledgment
This project was created by Jack McGowan, Eric Benett, Jackson Dean & Lucía Carrera.

## Resources
For the front end we have used code from which helped us with the creation of buttons: https://github.com/baraltech/Menu-System-PyGame/ 
To create the cards we have used code from: 
