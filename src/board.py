
import enum
from player import Player
from cards import Deck
from player import ComputerPlayer, User, Player
from enums import Rank, Suit, increment_rank

try:
    import sys
except ImportError as err:
    print("Fail loading a module in file:", __file__, "\n", err)
    sys.exit(2)

class BoardState(enum.Enum):
    INITIAL = 0
    PLAYING = 1
    FINISHED = 2


class Board:
    def __init__(self, num_players:int = 4, difficulty:int = 2) -> None:
        """
        ** params **
        * num_players: number of players in the game
        """
        self.state = BoardState.INITIAL

        players = [ComputerPlayer() for _ in range(num_players-1)] # all players but one are computers
        players.append(User()) # add a user player

        assert len(players) >= 3 and len(
            players) <= 6, "There must be between 3 and 6 players"

        self.players = players
        self.num_players = len(players)
        self.placed_pile = []

        self.lie = False

        self.winner = None

        self.deck = Deck()
        self.next_cards_to_play = []

        self.current_rank_to_play = Rank.ace
        self.previous_play_count = 0

        # get ready for new game
        self.deck.shuffle()
        self.deal_initial_cards()
        self.player_index_to_play = self.find_player_to_start()
        assert self.player_index_to_play is not None, "There must be a player with an ace of spades"

        self.state = BoardState.PLAYING

    def rank_to_play(self) -> int:
        return self.current_rank_to_play

    def find_player_to_start(self) -> int:
        """
        ** returns **
        * int : index of the player with the ace of spades, who will start the game
        """
        output = None
        for i, player in enumerate(self.players):
            for card in player.hand.get_cards():
                if card.get_rank() == Rank.ace and card.get_suit() == Suit.spades:
                    output = i
                    break
        return output

    def start_new_game(self) -> None:
        """ Starts a new game by resetting and shuffling the deck and dealing initial cards. 
        The first rank to be played is ace.
        The first player to play is the player with the ace of spades.
        """
        self.__init__(self.num_players)

    def deal_initial_cards(self) -> None:
        """Deals initial cards to each player."""
        player_deal_index = 0
        while len(self.deck.get_deck()) > 0:
            self.players[player_deal_index].hand.receive_card(
                self.deck.deal_card())
            
            # card is face up only when the player is the User
            self.players[player_deal_index].hand.get_cards()[-1].face_up = isinstance(self.players[player_deal_index], User)
        
            player_deal_index = (player_deal_index + 1) % self.num_players # next player

    def get_player_index_to_play(self) -> int:
        """Returns the player to play.
        ** returns **
        * Player object
        """
        return self.player_index_to_play

    def get_player_to_play(self) -> Player:
        """Returns the player to play.
        ** returns **
        * Player object
        """
        return self.players[self.player_index_to_play]

    def get_winner(self) -> Player:
        return self.winner
        
    def check_for_winner(self) -> Player:
        """Checks if there is a winner. Sets the state to FINISHED if there is a winner.
        ** returns **
        * player : player that won, None if no winner
        """
        # check if there is a winner
        for player in self.players:
            if (player.hand.get_num_cards() == 0) and player != self.get_player_to_play():
                self.state = BoardState.FINISHED
                self.winner = player
                return player
        return None

    def next_turn(self) -> None:
        """Progresses to the next turn by incrementing the current rank to be played
         and the player index to play.
         Any cards that are in the next_cards_to_play list are added to the placed pile.
         Checks for a winner
         """

        ### Store whether the last played hand was a lie or not
        self.lie = False
        for card in self.next_cards_to_play:
            if card.rank != self.current_rank_to_play:
                self.lie = True

        self.placed_pile += self.next_cards_to_play
        self.next_cards_to_play = [] # clear the queue of cards to play

        if self.check_for_winner() is not None:
            return # there is a winner

        self.current_rank_to_play = increment_rank(self.current_rank_to_play)
        self.player_index_to_play = (
            self.player_index_to_play - 1) % self.num_players

    def play_cards(self, cards) -> None:
        """Plays cards by adding them to the pile.
        ** params **
        * cards : List of card objects
        """
        self.next_cards_to_play += cards
        for c in self.next_cards_to_play:
            c.face_up = False # pile is face-down
        self.previous_play_count = len(self.next_cards_to_play)
            
        self.get_player_to_play().hand.remove_cards(cards)

    def give_pile_to_player(self, player) -> None:
        """Makes a player pick up the pile.
        ** params **
        * player : Player object: the player that either called BS and was wrong
         or was lying and had BS called on them (e.g. the player who has to pick up the pile)
        """
        for card in self.placed_pile[::-1]:
            card.face_up = isinstance(player, User) # only user cards are face-up
            player.hand.receive_card(card)
        
        # player also gets cards in the next_cards_to_play list
        for card in self.next_cards_to_play[::-1]:
            card.face_up = isinstance(player, User) # only user cards are face-up
            player.hand.receive_card(card)

        # clear the pile
        self.placed_pile = []
        self.next_cards_to_play = []

    # function to represent what happens when bullshit is called
    def bullshit_called(self, accuser, accused):
        # we need to know who called bullshit and how is the potential bullshitter 
        # if accused lied then they receive the stack of cards
        if self.lie:
            self.give_pile_to_player(accused)
        # if accused was honest then the accuser receives the stack
        else:
            self.give_pile_to_player(accuser)
        self.check_for_winner()
        # set up next turn (?)