import unittest
import sys

sys.path.insert(0, '.')
sys.path.insert(0, './src')

from app import App
from board import Board, BoardState
from cards import Card, Deck, Hand
from player import ComputerPlayer, User, Player
from enums import Rank, Suit
import os

class TestApp(unittest.TestCase):
    def test_create_app(self):
        # to prevent pygame from crashing on headless UNIX systems
        os.environ["SDL_VIDEODRIVER"] = "dummy" 
        
        # create a new app
        app = App()
        self.assertIsNotNone(app)

class TestPlayer(unittest.TestCase):
    def test_init(self):
        player = Player()
        self.assertEqual(len(player.hand.get_cards()), 0)
    
    def test_receive_card(self):
        # deal card and check that it is in the player's hand
        player = Player()
        player.receive_card(Card(Suit.spades, Rank.ace))
        self.assertEqual(len(player.hand.get_cards()), 1)
        self.assertEqual(player.hand.get_cards()[0].face_down, True)
        self.assertEqual(player.hand.get_cards()[0].get_rank(), Rank.ace) 
        self.assertEqual(player.hand.get_cards()[0].get_suit(), Suit.spades)
    
    def test_remove_cards(self):
        player = Player()
        # deal cards
        for i in range(14):
            player.receive_card(Card(i%4, i))
        self.assertEqual(len(player.hand.get_cards()), 14)
        # remove dealt cards
        for i, card in enumerate(player.hand.get_cards()[::-1]):
            player.remove_cards([card])
            self.assertEqual(len(player.hand.get_cards()), 13-i)
    
    def test_call_bullshit(self):
        computer = ComputerPlayer()
        user = User()
        # check that no errors are raised while calling bullshit
        computer.call_bullshit()
        user.call_bullshit()   
        
class TestCards(unittest.TestCase):
    def test_deck_methods(self):
        deck = Deck()
        self.assertEqual(len(deck.get_deck()), 52, "Deck must contain 52 cards") 
        deck.shuffle() # should not raise exception
        
        for card in deck.get_deck():
            self.assertEqual(card.face_down, True, "All cards must be face down")
        
        for i in range(52):
            deck.deal_card()
            
        self.assertEqual(len(deck.get_deck()), 0, "Deck must be empty after dealing all cards")
    
    def test_hand_methods(self):
        deck = Deck()
        hand = Hand([], True)
        last_card = deck.cards[-1]
        # test receive_card
        for i in range(13):
            hand.receive_card(deck.deal_card())
        self.assertEqual(len(hand.get_cards()), 13, "Hand must contain 13 cards")
        self.assertEqual(hand.get_cards()[0].get_rank(), last_card.get_rank(), "First card must be the same as the last card dealt")
        self.assertEqual(hand.get_cards()[0].get_suit(), last_card.get_suit(), "First card must be the same as the last card dealt")
        
        # test_remove_card
        for i in range(2):
            hand.remove_cards([hand.get_cards()[0]]) # remove first card
        self.assertEqual(len(hand.get_cards()), 11, "Hand must contain 11 cards after removing 2 cards")
        
        # test remove_cards
        hand.remove_cards(hand.get_cards()) # remove all
        self.assertEqual(len(hand.get_cards()), 0, "Hand must be empty after removing all cards")

    def test_card_methods(self):
        card = Card(Suit.spades, Rank.ace)
        self.assertEqual(card.get_rank(), Rank.ace)
        self.assertEqual(card.get_suit(), Suit.spades)
        self.assertTrue(card.face_down, "Card must be face down")
        card.flip()
        self.assertFalse(card.face_down, "Card must be face up after flipping")
         
class TestBoard(unittest.TestCase):
    def test_start_new_game(self):
        # Test with 5 players
        b = Board(5)
        assert b.players[0].hand.get_num_cards() == 11, "Player 1 should have 11 cards"
        assert b.players[1].hand.get_num_cards() == 11, "Player 2 should have 11 cards"
        assert b.players[2].hand.get_num_cards() == 10, "Player 3 should have 10 cards"
        assert b.players[3].hand.get_num_cards() == 10, "Player 4 should have 10 cards"
        assert b.players[4].hand.get_num_cards() == 10, "Player 5 should have 10 cards"
        assert isinstance(b.players[4], User), "Player 5 should be a User"

        # Test that cards are facing the correct way
        all_face_up = False
        for card in b.players[-1].hand.get_cards():
            all_face_up = all_face_up and not card.face_down 
        self.assertFalse(all_face_up, "User player should have all cards face up")
        
        all_face_down = True
        for card in b.players[0].hand.get_cards():
            all_face_down = card.face_down 
       
        self.assertTrue(all_face_down, "Computer player should have all cards face down")

    def test_next_turn(self):
        b = Board(4)
        assert b.current_rank_to_play == Rank.ace, "Current rank to play should be ace"
        b.next_turn()
        assert b.current_rank_to_play == Rank.two, "Current rank to play should be 2"
        for i in range(12):
            b.next_turn()
        assert b.current_rank_to_play == Rank.ace, "Current rank to play should be ace again"

    def test_play_card(self):
        # Test with 4 players
        b = Board(4)
        player_cards_before = b.get_player_to_play().hand.get_num_cards()
        pile_cards_before = len(b.placed_pile)
        b.play_cards(b.get_player_to_play().hand.get_cards()[0:2])
        assert len(b.next_cards_to_play) == 2, "There should be 2 cards in the queue"
        player_cards_after = b.get_player_to_play().hand.get_num_cards()
        b.next_turn()
        pile_cards_after = len(b.placed_pile)

        assert player_cards_before - player_cards_after == 2, "Player should have 2 less cards"
        assert pile_cards_before + 2 == pile_cards_after, "Pile should have 2 more cards"

        for card in b.placed_pile:
            assert card.face_up == False, "Pile should be face down"

    def test_pick_up_pile(self):
        b = Board(4)
        b.play_cards(b.get_player_to_play().hand.get_cards()[0:2]) # play 2
        b.next_turn()
        b.play_cards(b.get_player_to_play().hand.get_cards()[0:1]) # play 1
        b.next_turn()
        assert len(b.next_cards_to_play) == 0, "There should be no cards in the queue"
        assert len(b.placed_pile) == 3, "Pile should have 3 cards"

        player_cards_before = b.get_player_to_play().hand.get_num_cards()
        # example: player X called BS on the last player and they were lying ...
        b.give_pile_to_player(b.get_player_to_play())

        player_cards_after = b.get_player_to_play().hand.get_num_cards()
        assert player_cards_before + 3 == player_cards_after, "Player 1 should have 3 more cards than after playing"

    def test_check_winner(self):
        b = Board(4)
        assert b.state == BoardState.PLAYING, "Board should be in playing state"
        player = b.get_player_to_play()
        b.play_cards(player.hand.get_cards())
        b.next_turn() # player should win now
        ### Player will need to wait one more turn to win
        b.next_turn()
        assert b.state == BoardState.FINISHED, "Board should be in finished state"

if __name__ == '__main__':
    unittest.main()
    