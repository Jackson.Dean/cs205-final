"""
########## ------------------ > Import Statements < ------------------ ##########
"""
import math
from optparse import Option
from board import Board, BoardState
from cards import Card
from constants import NUM_PLAYERS, SCREEN_HEIGHT, SCREEN_WIDTH
from button import Button
from board import User, ComputerPlayer, Player
import enums
from options import Difficulty, Options
import random

try:
    import sys
    import constants as const
    import pygame
    import pygame.font
    from pygame.locals import (
        K_ESCAPE,
        KEYDOWN,
        QUIT,
        K_SPACE,
    )

except ImportError as err:
    print("Fail loading a module in file:", __file__, "\n", err)
    sys.exit(2)

"""
########## ------------------ > Define Classes < ------------------ ##########
"""

### Class to store PyGame clock information
class Clock:
    def __init__(self) -> None:
        self.c = pygame.time.Clock()
        self.counter = const.TURN_TIME
        self.timer_event = pygame.USEREVENT + 1 # Set variable for event checking
        pygame.time.set_timer(self.timer_event, 100) # Timer updates ever 100ms
    
    def get_clock(self) -> pygame.time.Clock:
        return self.c
    
    def get_counter(self) -> float:
        return self.counter

    def get_timer_event(self) -> pygame.event:
        return self.timer_event

    # Used to update the counter after it expires
    def set_counter(self, counter):
        self.counter = counter


### Class to store player card selection data
class Selection:
    def __init__(self) -> None:
        self.selected = []
    
    def get_selection(self) -> list:
        return self.selected
    
    # Make sure newly selected card not already in select, if so, remove it
    # as the card was highlighted, and user needs to remove it from selection
    def add_selection(self, index):
        if (index not in self.selected):
            self.selected.append(index)
        else:
            self.remove_selection(index)
    
    # remove card from selection
    def remove_selection(self, index):
        self.selected.remove(index)
    
    def clear_selection(self):
        self.selected = []

### Class for user card sprite to allow for collison mechanics when rendered
class Card_Sprite(pygame.sprite.Sprite):
    def __init__(self, img_path):
        pygame.sprite.Sprite.__init__(self)
        self.path = img_path
        self.image = pygame.image.load(img_path).convert_alpha()
        self.image = pygame.transform.scale(self.image, (const.CARD_WIDTH, const.CARD_HEIGHT))
        self.rect = self.image.get_rect()


### Main game runner class
class App:
    """The main application class, including the main game loop."""
    Instance = None


    """
    ########## ------------- > Basic Functions < ------------- ##########
    """
    def __init__(self) -> None:
        App.Instance = self
        self.running = True
        
        pygame.init() # initialize pygame
        self.screen = pygame.display.set_mode((const.SCREEN_WIDTH, const.SCREEN_HEIGHT))
        pygame.display.set_caption("Bullshit") # set window title
        self.animations = []
        self.ticks_last_frame = 0

    def run(self) -> None:
       # Main game loop
        while self.running:
            self.menu() 
        self.exit()
    
    def request_exit(self) -> None:
        self.running = False
    
    def exit(self) -> None:
        print("Thanks for playing!")

    ### Returns the file path for the requested card
    def get_card_image_path(self, card: Card) -> pygame.Surface:
        return "src/img/cards/" + enums.get_rank_string_from_enum(card.rank)+ "_of_" +  enums.get_suit_string_from_enum(card.suit)  + ".png"

    ### Checks for when the user clicks on a card in their hand, and updates the
    #   selection object
    def check_clicked_card(self, mouse_pos) -> pygame.sprite:
        index = 0
        if isinstance(self.board.get_player_to_play(), User):
            # Check if the user clicked on a card (only on their turn) 
            for card in self.player_hand:
                if card.rect.collidepoint(mouse_pos):
                    self.selected.add_selection(len(self.player_hand) - index - 1)
                    return card
                index += 1

    ### Builds the users selection, and submits that hand
    def submit_hand(self):
        user_hand = self.user.hand.get_cards()
        cards_to_play = []
        for c in self.selected.get_selection():
            cards_to_play.append(user_hand[c])

        self.board.play_cards(cards_to_play)
        self.animate_cards_to_stack([c for c in self.card_sprites if c.path in [self.get_card_image_path(p) for p in cards_to_play]])
        self.next_turn()

    def call_bullshit(self, accuser, accused):
        # Call Bullshit on the object (player or computer) passed in.
        if len(self.board.placed_pile):
            val = accuser.call_bullshit()
            if val:
                print("Bullshit was called")
                print("Accuser : " + str(accuser) + " | Accused : " + str(accused))
                self.board.bullshit_called(accuser, accused)
            self.check_winner()


    def computer_build_selection(self, computer) -> list:
        """Build a selection of cards for the computer to play.
        Always plays cards of the correct rank and has a chance to lie
        depending on the difficulty."""

        comp_cards = computer.hand.get_cards()

        selection = [c for c in comp_cards if c.get_rank() == self.board.rank_to_play()]
        
        # choose chance to lie based on difficulty
        if Options.difficulty == Difficulty.EASY:
            chance_to_lie = .15
            num_potential_lies = 1
        elif Options.difficulty == Difficulty.MEDIUM:
            chance_to_lie = .25
            num_potential_lies = 1
        elif Options.difficulty == Difficulty.HARD:
            chance_to_lie = .35
            num_potential_lies = 2

        if len(selection) == 0:
            # always lie if no cards of rank to play
            selection.append(comp_cards[0])

        for _ in range(num_potential_lies):
            if random.random() < chance_to_lie:
                chosen_card = random.choice(comp_cards)
                selection.append(chosen_card)
        selection = list(set(selection)) # remove duplicates
        return selection


    def computer_play_hand(self, counter):
        if isinstance(self.board.get_player_to_play(), ComputerPlayer):
            computer = self.board.get_player_to_play()
            max_value = max(math.floor(counter) * 4, 1)
            if counter > const.TURN_TIME - const.TURN_TIME/4:
                # Never play in the first fourth of turn time
                val = -1
            else:
                val = random.randint(1, max_value)
            if val == max_value:
                selection = self.computer_build_selection(computer)
                self.board.play_cards(selection)
                card_sprites = []
                for i in range(len(selection)):
                    sprite = Card_Sprite("src/img/back-side.png")
                    # start the animation at the computer's hand
                    sprite.rect.center = self.computer_coords[self.computers.index(computer)]
                    # add a random offset to the animation
                    sprite.rect.center = (sprite.rect.center[0] + const.CARD_WIDTH * i + random.random()*4,
                                          sprite.rect.center[1]+ random.random() * const.CARD_HEIGHT)
                    card_sprites.append(sprite)
                # animate playing cards
                self.animate_cards_to_stack(card_sprites)
                self.next_turn()
                


    def computer_call_bullshit(self, counter):
        """Decide if any of the computers will call bullshit."""
        computers = self.computers
        ### Check if player was last to play
        last_player = self.get_previous_player()
    
        valid_time_to_call = const.TURN_TIME - const.TURN_TIME/4

        if (self.board.get_player_index_to_play() != 0):
            ### Get previous player (whose hand is now the one on top of pile)
            if isinstance(last_player, ComputerPlayer):
                computers.remove(last_player)
        
        chance = self.get_bs_chance()

        # chance is per tick:
        chance = chance * self.deltaTime
        
        # don't call BS immediately
        if counter < valid_time_to_call and random.random() < chance:
            # one of the computers will call BS
            # pick one randomly
            self.call_bullshit(random.choice(computers), last_player)
                
    def get_bs_chance(self):
        """Calculate the chance of a computer calling bullshit."""
        # choose initial chance to call BS based on difficulty
        if Options.difficulty == Difficulty.EASY:
            chance = .1 * self.board.previous_play_count
        elif Options.difficulty == Difficulty.MEDIUM:
            chance = .15 * self.board.previous_play_count
        elif Options.difficulty == Difficulty.HARD:
            chance = .15 * self.board.previous_play_count
            if self.board.lie:
                # more likely if lie
                chance+=.1

        if self.board.previous_play_count > 5:
            # very likely to call bullshit
            chance = .95
        if self.board.previous_play_count > 6 and Options.difficulty != Difficulty.EASY:
            # always call bullshit
            chance = 200
        return chance
        
    def get_previous_player(self) -> Player:
        player_index = self.board.get_player_index_to_play() + 1
        if player_index == Options.num_players:
            player_index = 0
        
        return self.board.players[player_index]

        



    ### Function for the user to either call submit, or BS
    def pressed_game_button(self, player_object,text_in):
        if (text_in == "SUBMIT"):
            self.submit_hand()
            self.selected.clear_selection()
        else:
            self.call_bullshit(player_object, self.get_previous_player())
            
    ### Function to get the file path for the placed pile image
    def get_stack_image(self, num_cards):
        # load corresponding image
        if (num_cards <= 18):
            img = pygame.image.load( 'src/img/hands/HAND_' + str(num_cards) + '.png')
        else:
            img = pygame.image.load( 'src/img/hands/HAND_18.png')
        # scale image
        img = pygame.transform.scale(img, (const.HAND_WIDTH, const.HAND_HEIGHT))

        return img

    ### Function to progress the board onto the next turn, and reset timer
    def next_turn(self):
        self.board.next_turn()
        self.clock.set_counter(const.TURN_TIME)

    ### Function to get our Font asset
    def get_font(self, size): # Returns Press-Start-2P in the desired size
        return pygame.font.Font("src/assets/font.ttf", size)

    ### Function to check for the winner, and update the turn pointer
    def check_winner(self):
        if (self.board.state==BoardState.FINISHED):
            self.endscreen()

    def check_player_won(self):
        if self.board.get_winner() == self.user:
            return ["WINNER", const.Color.Gold]
        return ["YOU LOST", const.Color.LightGray]
    """
    ########## ------------- > Event Listener Functions < ------------- ##########
    """

    ### For The Menu Board
    def check_events(self, MENU_MOUSE_POS):
        """Check if the user triggered a UI event"""
        for event in pygame.event.get():
            # Check if the user hit a key
            if event.type == KEYDOWN:
                # If the key pressed was Escape, stop the game loop
                if event.key == K_ESCAPE:
                    self.running = False
                    return       

            # If the user clicked the window close button, stop the game loop
            if event.type == QUIT:
                self.running = False
                return

            if event.type == pygame.MOUSEBUTTONDOWN:
                self.check_menu_buttons(MENU_MOUSE_POS)

    def check_menu_buttons(self,MENU_MOUSE_POS):
        """Check if the user clicked on a button"""
        if self.MORE_PLAYERS_BTN.checkForInput(MENU_MOUSE_POS):
            Options.increase_num_players()
        elif self.LESS_PLAYERS_BTN.checkForInput(MENU_MOUSE_POS):
            Options.decrease_num_players()

        elif self.MORE_DIFFICULTY_BTN.checkForInput(MENU_MOUSE_POS):
            Options.increase_difficulty()
        elif self.LESS_DIFFICULTY_BTN.checkForInput(MENU_MOUSE_POS):
            Options.decrease_difficulty()

        elif self.PLAY_BTN.checkForInput(MENU_MOUSE_POS):
            self.game(Options.difficulty, Options.num_players)



    ### For The Game Board
    def game_check_events(self, GAME_MOUSE_POS):
        for event in pygame.event.get():
            # Check if the user hit a key
            if event.type == KEYDOWN:
                # If the key pressed was Escape, stop the game loop
                if event.key == K_ESCAPE:
                    self.running = False
                    break

            # If the user clicked the window close button, stop the game loop
            if event.type == QUIT:
                self.running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                self.game_check_mouse_down_events(GAME_MOUSE_POS)
        
            # If the event is the counter, update the Clock Object
            if event.type == pygame.USEREVENT+1:
                self.clock.set_counter(self.clock.get_counter() - 0.1)
                if self.clock.get_counter() < 0:
                    print('Time is up')
                    if self.board.get_player_to_play() == self.user:
                        self.board.state = BoardState.FINISHED # LOSE
                    self.next_turn()

    def game_check_mouse_down_events(self, GAME_MOUSE_POS):
        self.check_clicked_card(GAME_MOUSE_POS)
        try:
            if self.submit_btn.checkForInput(GAME_MOUSE_POS):
                self.pressed_game_button(self.user, self.submit_btn.text_input) # Either submits or calls BS
        except AttributeError:
            pass
        try:
            if self.bs_btn.checkForInput(GAME_MOUSE_POS):
                print("player click calling on BS button")
                ### Only check the BS button if it is not the players hand to be called on
                if (self.board.get_player_index_to_play() != (const.NUM_PLAYERS - 2)):
                    self.pressed_game_button(self.user, self.bs_btn.text_input) # Either submits or calls BS
        except AttributeError:
            pass

    """
    ########## ------------- > Render Functions < ------------- ##########
    """
    def render_num_player_settings(self):
        """Render the number of players settings"""
        self.PLAYERS_TEXT = self.get_font(20).render("PLAYERS", True, const.Color.White)
        PLAYERS_RECT = self.PLAYERS_TEXT.get_rect(center = (SCREEN_WIDTH/2, 200))
        self.screen.blit(self.PLAYERS_TEXT,PLAYERS_RECT)
        self.MORE_PLAYERS_BTN = Button(image=None, pos=(SCREEN_WIDTH/5 *3, 260), text_input="+", font=self.get_font(60), base_color="White", hovering_color="Green")
        self.LESS_PLAYERS_BTN = Button(image=None, pos=(SCREEN_WIDTH/5 *2, 260), text_input="-", font=self.get_font(60), base_color="White", hovering_color="Green")
        N_PLAYERS_TEXT = self.get_font(40).render(str(Options.num_players), True, const.Color.White)
        N_PLAYERS_RECT = self.PLAYERS_TEXT.get_rect(center = (SCREEN_WIDTH/2 + 50, 250))
        self.screen.blit(N_PLAYERS_TEXT,N_PLAYERS_RECT)

    def render_difficulty_settings(self):
        """Render the difficulty settings"""
        DIFFICULTY_TEXT = self.get_font(20).render("DIFFICULTY", True, const.Color.White)
        DIFFICULTY_RECT = self.PLAYERS_TEXT.get_rect(center = (SCREEN_WIDTH/2, 370))
        self.screen.blit(DIFFICULTY_TEXT,DIFFICULTY_RECT)
        self.MORE_DIFFICULTY_BTN = Button(image=None, pos=(SCREEN_WIDTH/5 *3.75, 430), text_input="+", font=self.get_font(60), base_color="White", hovering_color="Green")
        self.LESS_DIFFICULTY_BTN = Button(image=None, pos=(SCREEN_WIDTH/5 *1.75, 430), text_input="-", font=self.get_font(60), base_color="White", hovering_color="Green")
        L_DIFFICULTY_TEXT = self.get_font(40).render(str(Options.difficulty.name), True, const.Color.White)
        L_DIFFICULTY_RECT = self.PLAYERS_TEXT.get_rect(center = (SCREEN_WIDTH/2, 420))
        self.screen.blit(L_DIFFICULTY_TEXT,L_DIFFICULTY_RECT)


    
    def render_submit_btn(self, GAME_MOUSE_POS, TEXT_IN):
        self.submit_btn = Button(image=None, pos=(80, const.SCREEN_HEIGHT -50), text_input=TEXT_IN, font=self.get_font(25), base_color=const.Color.Red, hovering_color=const.Color.LightGray)
        self.submit_btn.changeColor(GAME_MOUSE_POS)
        self.submit_btn.update(self.screen)

    def render_bs_btn(self, GAME_MOUSE_POS, TEXT_IN):
        self.bs_btn = Button(image=None, pos=(const.SCREEN_WIDTH - 80, const.SCREEN_HEIGHT -50), text_input=TEXT_IN, font=self.get_font(25), base_color=const.Color.Red, hovering_color=const.Color.LightGray)
        self.bs_btn.changeColor(GAME_MOUSE_POS)
        self.bs_btn.update(self.screen)

    def render_player_to_play(self, positions):
        currInd = self.board.get_player_index_to_play()
        curr = positions[currInd]

        POINTER_TEXT = self.get_font(40).render(self.pointer_text, True, const.Color.White)
        POINTER_RECT = POINTER_TEXT.get_rect(center = (curr[0], curr[1]))
        self.screen.blit(POINTER_TEXT,POINTER_RECT)

    def render_placed_pile(self):
        self.placed_pile = self.board.placed_pile
        num_cards = len(self.placed_pile)
        if num_cards==0:
            img = pygame.image.load( 'src/img/stack.png')
            self.pile_image = pygame.transform.scale(img, (const.CARD_WIDTH, const.CARD_HEIGHT))
        else:
            self.pile_image = self.get_stack_image(num_cards)
        self.pile_rect = self.pile_image.get_rect(center = (const.SCREEN_WIDTH / 2, const.SCREEN_HEIGHT / 2))
        self.screen.blit(self.pile_image, self.pile_rect)



    def create_user_player(self, positions, selection) -> list:
        # need to get amount of cards user has and which cards they are
        cards = self.user.hand.get_cards()
        self.card_sprites = []
        num_cards = len(cards)

        selected = [0] * num_cards
        for i in selection.get_selection():
            selected[i] = 1

        # constants
        HEIGHT_FIRST_ROW = const.SCREEN_HEIGHT/4*3 
        HEIGHT_SECOND_ROW = const.SCREEN_HEIGHT/4*3 - 100
        OFFSET = 30
        width = const.SCREEN_WIDTH/2 - (num_cards/2*const.CARD_WIDTH)/2
        height = const.SCREEN_HEIGHT - const.CARD_HEIGHT - const.TOP_MARGIN/6

        for j in range(0, len(cards)):
            # get card num + rank

            img = self.get_card_image_path(cards[j])
            outline = pygame.image.load("src/img/cards/outline.png")
            outline = pygame.transform.scale(outline, (const.CARD_WIDTH, const.CARD_HEIGHT))

            # load corresponding image
            card = Card_Sprite(img)

            card.rect.topleft = (width, height)
            card.rect.bottomright = (width + const.CARD_WIDTH, height + const.CARD_HEIGHT)
            self.card_sprites.append(card)
    
            self.screen.blit(card.image, (width, height))
            if selected[j] == 1:
                self.screen.blit(outline, (width, height))
            width += OFFSET

        cords = [const.SCREEN_WIDTH/2, height - 10]
        positions.append(cords)
        
        return self.card_sprites

    def create_computer_players(self, players, positions):
        heights = []
        widths = []
        self.computers = [] # add all the computer players surfaces here

        # arrays with appropriate heights and widths are saved in constants for each setting of player number
        if players == 3:
            heights = const.THREE_PLAYERS_HEIGHTS
            widths = const.THREE_PLAYERS_WIDTHS
        elif players == 4: # right now I just have it setup for four players
            heights = const.FOUR_PLAYERS_HEIGHTS
            widths = const.FOUR_PLAYERS_WIDTHS
        elif players == 5:
            heights = const.FIVE_PLAYERS_HEIGHTS
            widths = const.FIVE_PLAYERS_WIDTHS
        else:
            heights = const.SIX_PLAYERS_HEIGHTS
            widths = const.SIX_PLAYERS_WIDTHS
        
        # remember user player already created
        # create computer player surfaces and add them to screen
        # loop through the widths and heights array so they have the correct positioning
        j = 0
        for i in range(0,players):
            if isinstance(self.board.players[i], User) == False:
                self.computers.append(self.board.players[i])
            else:
                self.user = self.board.players[i]
        
        self.computer_surfs = []
        # create surfaces
        for j in range(0, len(self.computers)):
            # get amount of cards dealt to computer player
            num_cards = len(self.computers[j].hand.get_cards())

            # Load corresponding image to num cards and append to computer card surfs
            self.computer_surfs.append(self.get_stack_image(num_cards))
            # rotate image
            positions.append([widths[j] + const.HAND_WIDTH/2, heights[j] - 10])
            # distance from center of screen:
            rel_x, rel_y = positions[j]
            # get center of screen
            center_x, center_y = const.SCREEN_WIDTH/2, const.SCREEN_HEIGHT/2
            # get distance from center of screen
            x_dist = rel_x - center_x
            y_dist = rel_y - center_y
            # get angle
            angle = math.atan2(y_dist, x_dist)
            # rotate image
            self.computer_surfs[j] = pygame.transform.rotate(self.computer_surfs[j], 90-math.degrees(angle))
            # position image
            self.screen.blit(self.computer_surfs[j], (widths[j], heights[j]))
        self.computer_coords = positions
        
    def animate_cards_to_stack(self, cards):
        # animate cards
        for card in cards:
            start = card.rect.center
            end = self.pile_rect.center
            self.animations.append((card, start, end, const.CARD_PLAY_ANIM_LENGTH))
            
    def update_animations(self):
        for i, anim in enumerate(self.animations):
            card = anim[0]
            start = anim[1]
            end = anim[2]
            time_left = anim[3]
            self.animations[i] = (card, start, end, time_left - self.deltaTime)
            if time_left <= 0:
                if anim in self.animations:
                    self.animations.remove(anim)
            else:

                # animate over 3 seconds
                delta_x = self.deltaTime * (end[0] - start[0]) / const.CARD_PLAY_ANIM_LENGTH
                delta_y = self.deltaTime * (end[1] - start[1]) / const.CARD_PLAY_ANIM_LENGTH

                card.rect.center = (card.rect.center[0] + delta_x, card.rect.center[1] + delta_y)
                self.screen.blit(card.image, card.rect)

    """
    ########## ------------- > Board View Functions < ------------- ##########
    """
    # menu screen
    def menu(self):
        """Renders and updates the menu screen"""
        while self.running:
            self.screen.fill(const.Color.Red)
            MENU_MOUSE_POS = pygame.mouse.get_pos()

            # Game title
            MENU_TEXT = self.get_font(70).render("BULLSHIT", True, const.Color.White)
            MENU_RECT = MENU_TEXT.get_rect(center = (SCREEN_WIDTH/2, 100))
            self.screen.blit(MENU_TEXT,MENU_RECT)

            # Render menu options
            self.render_num_player_settings()
            self.render_difficulty_settings()

            # Play button
            self.PLAY_BTN = Button(image=None, pos=(SCREEN_WIDTH/2, SCREEN_HEIGHT - 50), text_input="PLAY", font=self.get_font(50), base_color= const.Color.Green, hovering_color="White")

            # update buttons
            for button in [self.MORE_PLAYERS_BTN, self.LESS_PLAYERS_BTN, self.MORE_DIFFICULTY_BTN, self.LESS_DIFFICULTY_BTN, self.PLAY_BTN]:
                button.changeColor(MENU_MOUSE_POS)
                button.update(self.screen)

            # check events
            self.check_events(MENU_MOUSE_POS)
            
            # Update display
            pygame.display.update()


    # function to setup board
    def game(self, difficulty, players):
        """Renders and updates the game screen"""
        # save menu settings and create board
        self.board = Board(players, difficulty)
        # loop game

        # Creates the clock
        self.clock = Clock()
        self.selected = Selection()

        self.pointer_text = "*"
        while self.running:
            t = pygame.time.get_ticks()
            # deltaTime in seconds.
            self.deltaTime = (t - self.ticks_last_frame) / 1000.0
            self.ticks_last_frame = t
            # Create a surface, which represents the VIEW component of MVC of (width, height)
            # This surface will function as the "root" display
            # creates a green background
            self.screen.fill(const.Color.Green)
            GAME_MOUSE_POS = pygame.mouse.get_pos()

            # setup buttons
            
            if isinstance(self.board.get_player_to_play(), User):
                if len(self.selected.get_selection()) > 0:
                    self.render_submit_btn(GAME_MOUSE_POS, "SUBMIT")
            if self.board.get_player_index_to_play() != (const.NUM_PLAYERS - 2):
                self.render_bs_btn(GAME_MOUSE_POS, "BS")

            ### Store player positions
            self.positions = []

            # create computer players cards
            self.create_computer_players(players, self.positions)

            # create player cards
            self.player_hand = self.create_user_player(self.positions, self.selected)

            ### Reverse the player hand for priority list
            self.player_hand.reverse()

            ### Render the Icon for which player is currently playing
            self.render_player_to_play(self.positions)


            ### Setup clock for the board
            counter = self.clock.get_counter()


            curr_rank = self.board.rank_to_play()
            curr_rank = enums.get_rank_string_from_enum(curr_rank)
            TURN_TEXT = self.get_font(20).render("TO PLAY: " + curr_rank, True, const.Color.White)
            TURN_RECT = TURN_TEXT.get_rect(center = (SCREEN_WIDTH/2, SCREEN_HEIGHT/2 - 80))
            self.screen.blit(TURN_TEXT, TURN_RECT)


            ### Place Turn Timer
            TIMER_TEXT = self.get_font(30).render(f'{counter:.1f}', True, const.Color.White)
            TIMER_RECT = TIMER_TEXT.get_rect(center = (60, 20))
            self.screen.blit(TIMER_TEXT, TIMER_RECT)

            ### Call Computer Play
            self.computer_play_hand(counter)

            self.computer_call_bullshit(counter)


            # Create stack
            ### Render placed pile
            self.render_placed_pile()
            
            self.update_animations()

            self.check_winner()

            self.game_check_events(GAME_MOUSE_POS)
                            
            pygame.display.update()

    def endscreen(self):
        """Renders and updates the menu screen"""
        while self.running:
            self.screen.fill(const.Color.Red)
            END_MOUSE_POS = pygame.mouse.get_pos()

            # Game title
            win_lose, text_color = self.check_player_won()

            MENU_TEXT = self.get_font(50).render(win_lose, True, text_color)
            MENU_RECT = MENU_TEXT.get_rect(center = (SCREEN_WIDTH/2, 100))
            self.screen.blit(MENU_TEXT,MENU_RECT)

            # Render menu options
            self.render_num_player_settings()
            self.render_difficulty_settings()

            # Play button
            self.PLAY_BTN = Button(image=None, pos=(SCREEN_WIDTH/2, SCREEN_HEIGHT - 50), text_input="PLAY AGAIN", font=self.get_font(50), base_color= const.Color.Green, hovering_color="White")

            # update buttons
            for button in [self.MORE_PLAYERS_BTN, self.LESS_PLAYERS_BTN, self.MORE_DIFFICULTY_BTN, self.LESS_DIFFICULTY_BTN, self.PLAY_BTN]:
                button.changeColor(END_MOUSE_POS)
                button.update(self.screen)

            # check events
            self.check_events(END_MOUSE_POS)
            
            # Update display
            pygame.display.update()



if __name__ == "__main__":
    app = App()
    app.run()