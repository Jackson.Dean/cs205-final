
### Source from `Vannov` in GitHub project 'pygame_cards'

try:
    import sys
except ImportError as err:
    print("Fail loading a module in file:", __file__, "\n", err)
    sys.exit(2)


class Rank:
    """ Enums for cards' ranks """
    ace = 1
    two = 2
    three = 3
    four = 4
    five = 5
    six = 6
    seven = 7
    eight = 8
    nine = 9
    ten = 10
    jack = 11
    queen = 12
    king = 13


class Suit:
    """ Enums for cards' suits """
    hearts = 0
    diamonds = 1
    clubs = 2
    spades = 3

def get_rank_string_from_enum(rank):
    ranks = ['ace','2','3','4','5','6','7','8','9','10','jack','queen','king']
    return ranks[(rank - 1)] # Rank of ace (1) will return index of 0.

def get_suit_string_from_enum(suit):
    """ Returns unicode character of a card suit
    :param suit: int value that corresponds to one of enums from Suit class
    :return: string with unicode character of the corresponding card suit.
    Empty string if passed suit is not a valid Suit enum
    """
    if suit == Suit.hearts:
        return 'hearts'
    elif suit == Suit.diamonds:
        return 'diamonds'
    elif suit == Suit.clubs:
        return 'clubs'
    elif suit == Suit.spades:
        return 'spades'
    else:
        return ''

def get_suit_icon_from_enum(suit):
    """ Returns unicode character of a card suit
    :param suit: int value that corresponds to one of enums from Suit class
    :return: string with unicode character of the corresponding card suit.
    Empty string if passed suit is not a valid Suit enum
    """
    if suit == Suit.hearts:
        return '\u2665'
    elif suit == Suit.diamonds:
        return '\u2666'
    elif suit == Suit.clubs:
        return '\u2663'
    elif suit == Suit.spades:
        return '\u2660'
    else:
        return ''

def increment_rank(rank):
    """ Increments rank by 1. If rank is ace, returns 2
    :param rank: current Rank 
    :return: Rank: the next rank. If passed rank is not a valid Rank enum, returns None
    """
    ranks = [Rank.ace, Rank.two, Rank.three, Rank.four, Rank.five, Rank.six, Rank.seven, Rank.eight, Rank.nine, Rank.ten, Rank.jack, Rank.queen, Rank.king]
    current_rank_index = rank - 1
    next_rank_index = current_rank_index + 1
    if next_rank_index >= len(ranks):
        next_rank_index = 0
    return ranks[next_rank_index]
