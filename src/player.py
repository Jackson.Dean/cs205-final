try:
    import sys
    import cards as CARDS
    import random
    
except ImportError as err:
    print("Fail loading a module in file:", __file__, "\n", err)
    sys.exit(2)

# has a hand of cards
class Player:
    def __init__(self) -> None:
        self.hand = CARDS.Hand([], True)

    def receive_card(self, card) -> None:
        self.hand.receive_card(card)

    def remove_cards(self, cards) -> None:
        self.hand.remove_cards(cards)
    
    # this should belong in board because it keeps track of who calls bullshit and the logic behind it 
    # belongs in board (checking to see if the bullshit call is actually true, who called it, etc)
    def call_bullshit(self) -> bool:
        pass # override this method in the subclass

class User(Player):
    def __init__(self) -> None:
        super().__init__()
    
    """ If the User wants to Call BS, just do it """
    def call_bullshit(self) -> bool:
        return True

class ComputerPlayer(Player):
    def __init__(self) -> None:
        self.CHANCE_OF_BS = 15
        super().__init__()

    def call_bullshit(self) -> bool:
        chance = random.randint(1,self.CHANCE_OF_BS)
        if (chance == 1):
            return True
        return False

    

