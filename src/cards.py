from array import array
import string
from xmlrpc.client import Boolean


try:
    import sys
    from random import shuffle

    import enums
except ImportError as err:
    print("Fail loading a module in file:", __file__, "\n", err)
    sys.exit(2)

class Card:
    def __init__(self, suit, rank, face_down=True) -> None:
        self.suit = suit
        self.rank = rank
        self.face_down = face_down

    def get_card(self) -> str:
        return enums.get_rank_string_from_enum(self.rank) + " of " + enums.get_suit_string_from_enum(self.suit)

    def get_suit(self) -> int:
        return self.suit
    
    def get_suit_string(self) -> str:
        return enums.get_suit_string_from_enum(self.suit)

    def get_rank(self) -> int:
        return self.rank

    def get_rank_string(self) -> str:
        return enums.get_rank_string_from_enum(self.rank)

    def get_face_down(self) -> bool:
        return self.face_down

    def flip(self) -> None:
        self.face_down = not self.face_down


class Deck:
    def __init__(self, last_card_callback=None) -> None:
        self.cards = []
        for rank in range(enums.Rank.ace, enums.Rank.king + 1):
            for suit in range(enums.Suit.hearts, enums.Suit.spades + 1):
                self.cards.append(Card(suit, rank))
        shuffle(self.cards)

    def get_deck(self) -> list:
        return self.cards

    def deal_card(self) -> object:
        return self.cards.pop()
        
    def shuffle(self) -> None:
        """ Reinitalize the deck (so it's full) and then shuffle it again (done in the init)"""
        self.__init__()

class Hand:
    def __init__(self, cards, is_true) -> None:
        self.cards = cards
        self.num_cards = len(cards)
        self.is_true = is_true
    
    def get_num_cards(self) -> int:
        return self.num_cards

    def get_cards(self) -> list:
        return self.cards
    
    def get_is_true(self) -> bool :
        return self.is_true

    def receive_card(self, card) -> None:
        self.cards.append(card)
        self.num_cards = len(self.cards)

    def remove_card(self, card) -> None:
        self.cards.remove(card)
        self.num_cards = len(self.cards)

    def remove_cards(self, cards) -> None:
        self.cards = [card for card in self.cards if card not in cards]
        self.num_cards = len(self.cards)



