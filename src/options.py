
from enum import Enum, unique

@unique # no repeat difficult values
class Difficulty(Enum):
    EASY = 1
    MEDIUM = 2
    HARD = 3

class classproperty(staticmethod):
    def __get__(self, *_):         
        return self.__func__()
        
class Options():
    # default values
    __difficulty = Difficulty.MEDIUM
    __num_players = 4 
    
    @classproperty
    def difficulty():
        return Options.__difficulty
    
    @classproperty
    def num_players():
        return Options.__num_players
    
    @staticmethod
    def increase_difficulty():
        if Options.__difficulty.value < Difficulty.HARD.value:
            Options.__difficulty = Difficulty(Options.__difficulty.value+1)
            
    @staticmethod
    def decrease_difficulty():
        if Options.__difficulty.value > Difficulty.EASY.value:
            Options.__difficulty = Difficulty(Options.__difficulty.value-1)
    
    @staticmethod
    def set_difficulty(difficulty):
        if difficulty.value > max([d.value for d in Difficulty]):
            print("Difficulty is too high")
            return
        if difficulty.value < min([d.value for d in Difficulty]):
            print("Difficulty is too low")
            return
        Options.difficulty = difficulty
    
    @staticmethod
    def increase_num_players():
        if Options.__num_players < 6:
            Options.__num_players += 1
            
    @staticmethod
    def decrease_num_players():
        if Options.__num_players > 3:
            Options.__num_players -= 1
   
    @staticmethod
    def set_num_player(num_players):
        if num_players > 6:
            print("Num players is too high")
            return
        if num_players < 3:
            print("Num players is too low")
            return
        Options.__num_players = num_players